/* global EntityCollection, bobbel_sensors, bobbel_entities, Log, Tank, Simulator, Entity */

//TIP: If you disable caching in webbrowser. Live.js reloads page on any changes of these files automatically
'use strict';

/**
 * @author Robin Nicolay <robin.nicolay@uni-rostock.de>
 */

/**
 * This function is called by Simulator automatically when program loaded or if "Restart" pressed
 *
 * Adds Entities defined in properties.js to Simulator
 *      EntityCollection.setEntities(bobbel_entities, bobbel_sensors);
 * and adds Edges defined in properties.js to Simulator
 *      EdgeCollection.setEdges(bobbel_edges);
 *
 * You can perform additional actions for startup here if you wish
 */
var load_bobbel_data = function(){

    /** place init code here **/

    EntityCollection.setEntities(bobbel_entities, bobbel_sensors);
    EdgeCollection.setEdges(bobbel_edges);

    /** or here **/

    Log.info('Initialization done!');
};

/**
 * This function is not meant to be called by you. Called in preparation of every Simulation step
 * Perform preparation steps here if you like

 * @param entity_list list of all entities that are included to simulation
 * @param step_count number of simulation step
 *
 */
var perform_simulation_step_initialization = function(entity_list, step_count){

};

/**
 * This function is not meant to be called by you. It is called automatically during Simulation step for every Entity-Object in EntityCollection
 * Perform you simulation code for every entity here!
 *
 * @param entity reference to current Entity-Object (see README.md)
 * @param perceptions object mapping sensornames to lists of perceived entities or null if empty (see README.md)
 * @param step_count number of simulation step
 */
var perform_simulation_step_on_entity = function(entity, perceptions, step_count){

    //this is simple demo code

    var text = "";

    if (perceptions) {

        // if there are any perceptions add them as strings into text variable
        for (var sensorTag in perceptions) {
            for (var index in perceptions[sensorTag]){
                var perception = perceptions[sensorTag][index];

                // a perception can have different types. Entities such as other bobbels...
                if (perception['type'] === 'Entity-Object') {
                    text += ((sensorTag + "'s " + perception['object']['name']
                        + " " + Math.round(perception['distance']) + "px"
                        + " " + Math.round(perception['direction']) + "°)\n"));
                }

                // ... or other edges
                if (perception['type'] === 'Edge-Object'){
                    text += ((sensorTag + "'s " + perception['object']['name'] + "\n"));

                    // if the perception is a edge, there are intersection points (where edge hit sensorTag polygons).
                    // this paints the yellow line between these points
                    var intersectionsList = perception['sensor_intersections'];
                    if (intersectionsList.length === 2) {
                        Tank.displayEdge(intersectionsList[0], intersectionsList[1], 'yellow');
                    }
                }
            }
        }

        entity.showTime = 50; // you can store arbitrary states in a entity. this is one
        entity.text = text; // entities have relevant internal states (see documentation) text is one. It contains the text next to an entity

    } else {
        // if there is now perception it counts down the "showTime" property and then removes the text
        if (entity.showTime)
            entity.showTime--;
        else
            entity.text = "";
    }

    // depending on if there is a perception it behaves in different ways. this is a simple example of random movement
    if (!perceptions) {
        if (Math.random() > 0.5) {
            entity.rotate(5);
        } else {
            entity.rotate(-5);
        }
        entity.move(1);
    } else {
        if (Math.random() > 0.5) {
            entity.rotate(20);
        } else {
            entity.rotate(-20);
        }
        entity.move(-1);
    }
};

/**
 * This function is not meant to be called by you. It is called automatically for finalization at the end of every simulation step. Changes to visualization are performed afterwards
 * Perform you finalization code here if you like!
 *
 * @param entity_list list of all entities
 * @param step_count number of simulation step
 * @param duration time simulation took
 */
var perform_simulation_step_finalization = function(entity_list, step_count, duration) {
    Log.debug('Performed simulation step ' + step_count + ' for ' + duration + 'ms', 1, "simulator_performing_step");
};